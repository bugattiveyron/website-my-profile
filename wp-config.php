<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_teste');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'H#?ko]BIn$;9{?b`ELA[,Xr:qBCDuOlMcAVva~U$QqlAe_iPC?:&tOY%Iy*$+B,2');
define('SECURE_AUTH_KEY',  '!?BW+F>nvq]A:8CItCOv(&w[7HVxw/J$I;qo[q9_6i<c*8IzcQ.T0/gImMg%0{Hb');
define('LOGGED_IN_KEY',    'm4=47R#XQzL[JX%U-T@SRC)A@]Mr`|mGs,mceqW]M3oiqr;Y,4$BKJdE@LNF[|~5');
define('NONCE_KEY',        'MyXjMu0TkXUGdi.BZ=L(_KqSZJv,)+X}T1/QM+YTdN$y7(rZS4sQ:~gNNdrRiYS,');
define('AUTH_SALT',        '@dQtipn7YdI)jmVSIGR`9##FCDJSF[;h nh7~k !X&?v6{JH|pZ81e~KmLZYWI2N');
define('SECURE_AUTH_SALT', 'Z+kGXx^Bj4BHR|~mZWXul3183%Q)GD[Qw[[/](G]o.eO-<4Ien/jNf}>Vs_:(Fd ');
define('LOGGED_IN_SALT',   '$&cHmFP.PhK@<WjjpT!3+8E%/E3S}+z.<3go?lwPGIM4!}jzU-Qsqr2vr$~l~jgp');
define('NONCE_SALT',       '03&/A/UTJhh CA=Om%]y34[W{yHL!El>aJ ONV2A09$2rt3GO`/|b$n}1yLHZ~hR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WPLANG', 'pt_BR');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
